import './App.css';
import Developer from './components/Developer'

function App() {
  return (
    <div className="App">
      <Developer 
      name = 'Douglas' 
      idade= {22}
      pais = 'Brasil'/>


      <Developer 
      name = 'Filipe' 
      idade= {21}
      pais = 'Japão'/>


      <Developer 
      name = 'Mario' 
      idade= {19}
      pais = 'Itália'/>
    </div>

    
  );
}

export default App;
