import './style.css'

function Developer(props){
    return(
        <div className = 'dev'>
            <h2>Dev: {props.name}</h2>
            <h3>Idade: {props.idade}</h3>
            <h3>País: {props.pais}</h3>
        </div>
    )
}

export default Developer